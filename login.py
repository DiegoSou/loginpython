import hashlib

cadastros = {
    "Admin" : hashlib.md5(("123456").encode()).digest(),
    "Cliente1" : hashlib.md5(("usosudo").encode()).digest()
}

def login(user) :

    if user in cadastros :
        if checkPass(user) :
            return True
        else :
            for i in range(2) :
                if checkPass(user) :
                    return True
    else :
        return False


def checkPass(user) :
    senha = hashlib.md5(input("Digite a senha: ").encode()) 
    if senha.digest() in cadastros.values() :
        for k in cadastros.keys() :
            if cadastros[user] == cadastros[k] and cadastros[k] == senha.digest():
                return True